<html lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <head>
	<title>A manifest to the madness</title>
	<style>
	    *{
		box-sizing: border-box;
		padding: 0;
		margin: 0;
	    }

	    /* Set height of body and the document to 100% */
	    body, html {
		height: 100%;
		margin: 0;
		font-family: Arial, Helvetica, sans-serif;
	    }

	    /* Style tab links */
	    .tablink {
		background-color: #555;
		color: black;
		float: left;
		border: none;
		outline: none;
		cursor: pointer;
		padding: 14px 16px;
		font-size: 17px;
		width: 25%;
	    }

	    .tablink:hover {
		background-color: #777;
	    }

	    /* ---------------------------------------------------------------- */

	    /* Style the tab content (and add height:100% for full page content) */
	    .tabcontent {
		color: black;
		display: flex;
		padding: 50px 20px;
		height: 100%;
		width: 100%;
		float: right;
		box-sizing: border-box;
	    }

	    #Home {background-color: tomato;}
	    #CV {background-color: lightgreen;}
	    #PersProj {background-color: lightblue;}
	    #SchProj {background-color: darkorange;}

	    /* ------------------------------------------------------------------ */
	    
	    .tabcontent .sidebar{
        height: 100%;
        width: 20%;
        position: static;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        padding-top: 20px;
        float: left;
        box-sizing: border-box;
	    }
	    
	    .tabcontent .sidebar a {
		padding: 6px 8px 6px 16px;
		text-decoration: none;
		font-size: 25px;
		color: #818181;
		display: block;
	    }

	    .tabcontent .sidebar a:hover {
		    color: #f1f1f1;
	    }

	    /* ----------------------------------------------------------------- */	
	    
	    .clr{
		clear:both;
	    }
	    
	    /* ----------------------------------------------------------------- */

	    aside{
		float: left;
	    }

	</style>
    </head>
    <body>
		<!--Tab buttons -->
		<button class="tablink" onclick="openPage('Home', this, 'tomato')" id="defaultOpen">Home</button>
		<button class="tablink" onclick="openPage('CV', this, 'lightgreen')">Curriculum Vitae</button>
		<button class="tablink" onclick="openPage('PersProj', this, 'lightblue')">Personal Projects</button>
		<button class="tablink" onclick="openPage('SchProj', this, 'darkorange')">School Projects</button>

		<div class="clr"></div>

		<div id="Home" class="tabcontent">
			<h1>Home</h1>
			<p><i>Home is where the heart is..</i></p>
		</div>

		<div id="CV" class="tabcontent">   
			<div class="sidebar">
				<a href="#General Info" id="GI">General Information</a>
				<a href="#Education" id="Edu">Education</a>
				<a href="#Skills" id="Ski">Skills</a>
				<a href="#Experience" id="Exp">Experience</a>
				<a href="#Hobbies" id="Hob">Hobbies</a>
			</div>
			<div class="container" > 
				<h1>Curriculum Vitae</h1>
			
				<p><i>The story of my life ... litteraly</i></p>
			</div>	
		</div>

		<div id="PersProj" class="tabcontent">
			<h1>Personal Projects</h1>
				<p><i>Failure paves the road to succes</i></p>
		</div>

		<div id="SchProj" class="tabcontent">
			<h1>School Projects</h1>
				<p><i>We do what we have to, to get better at what we want to</i></p>
		</div>

		<div class="clr"></div>

		<!-- tabbing script -->
		<script>
			function openPage(pageName,elmnt,color) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
				for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
				tablinks = document.getElementsByClassName("tablink");
				for (i = 0; i < tablinks.length; i++) {
			tablinks[i].style.backgroundColor = "";
			}
			document.getElementById(pageName).style.display = "block";
			elmnt.style.backgroundColor = color;
			}

			// Get the element with id="defaultOpen" and click on it
			document.getElementById("defaultOpen").click();
		</script>

		<!-- footer -->
		<footer class="w3-container w3-padding-64 w3-center w3-black w3-xlarge">
			<a href="#"><i class="fa fa-facebook-official"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			
			<p class="w3-medium"> Made by 
			<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank">Stijn Boons</a>
			</p>
		</footer>
    </body>
</html>