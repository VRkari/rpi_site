#include <iostream>
#include <string>

int main(){

	int num;

	std::cout<<"Enter the a positive integer to which the program will count: ";
	std::cin >> num;

	for(int i = 1; i <= num; i++){

		if(i % 3 == 0 && i % 5 == 0){
			std::cout << i << " = fizzbuzz" << std::endl;
		}
		else if( i % 3 == 0 && i % 5 != 0){
			std::cout << i  << " = fizz" << std::endl;
		}

		else if( i % 3 != 0 && i % 5 == 0){
			std::cout << i << " = buzz" << std::endl;
		}
		else
			std::cout << i << std::endl;
	}
}
