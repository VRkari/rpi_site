const navSlide = () => {
    const collapse_menu = document.querySelector('.collapse_menu');
    const nav = document.querySelector('.topnav');
    const menu_options = document.querySelectorAll('.topnav li');

    collapse_menu.addEventListener('click', ()=>{
        //toggle collapse menu
        nav.classList.toggle('nav-active');

        //animate menu options
        menu_options.forEach((link, index)=>{
            if (link.style.animation){
                link.style.animation = '';
            } else {
                link.style.animation =  `topnav_fade 0.2s ease forwards ${index/7+0.4}s`;
            }
        });
        //topnav animation
        collapse_menu.classList.toggle('toggle');
    });
}

navSlide();