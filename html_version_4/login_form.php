<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles_login.css">
</head>
<body>
    <h2>Modal Login Form</h2>

    <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Login</button>
    <div id="id01" class="modal"> 
        <form class="modal-content animate" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
            <div class="imgcontainer">
                <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
                <img src="img_avatar2.png" alt="Avatar" class="avatar">
            </div>
            <div class="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" required>  
                <button type="submit">Login</button>
            </div>
            <div class="container" style="background-color:#f1f1f1">
                <button type="button" onclick="document.getElementById('id01').style.display='none'" 
                class="cancelbtn">Cancel</button>
                <span class="psw">Forgot <a href="#">password?</a></span>
            </div>
        </form>
    </div>

    <?php
    // define variables and set to empty values
    $nameErr = $passwordErr = "";
    $name = $password = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["uname"])) {
            $nameErr = "Name is required";
        } else {
            $name = test_input($_POST["uname"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed";
            }
        }
            
        if (empty($_POST["psw"])) {
            $password = "";
        } else {
            $password = test_input($_POST["psw"]);
            // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
            if (!preg_match("/^[-a-z0-9+&@#\/%?=~_|!:,.;]*$/",$password)) {
            $passwordErr = "Invalid password";
            }
        }

        $host = "localhost";
        $dbUsername = "raikari";
        $dbPassword = "@Konijntje9";
        $dbname = "Website_data";

        $conn = new mysqli($host, $dbUsername, $dbPassword, $dbname);

        if (mysqli_connect_error()) {
            die('Connection Error('. mysqli_connect_errno().')'. mysqli_connect_error());
        } else {
            $SELECT = "SELECT User From Login_data Where User = ? Limit 1";
            $INSERT = "INSERT Into register (name, password) values(?, ?)";

            $stmt = $conn->prepare($SELECT);
            $stmt->bind_param("s", $name);
            $stmt->execute();
            $stmt->bind_result($name);
            $stmt->store_result();
            $rnum = $stmt->num_rows;
            if ($rnum == 0) {
                $stmt->close();
                $stmt = $conn->prepare($INSERT);
                $stmt->bind_param("ss", $name, $password);
                $stmt->execute();
                echo "new userdata stored sucessfully";
            } else {
                echo "Already someone registered with this username";
            }
            $stmt->close();
            $conn->close();
        }
    } else {
        echo "All fields required";
        die();
    }
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>


    <script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    </script>

</body>
</html>
